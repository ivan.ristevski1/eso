<?php

namespace App\Controller;

use App\Entity\Movie;
use Couchbase\SearchQuery;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\OptionsResolver\OptionsResolver;


class MovieController extends AbstractController
{
    /**
     * @param Request $request
     * @Route("/", name="movie")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function indexSearch(Request $request){
        $i=1;
        $js = array();
        $ss = array();
        $rt = array();
        $url ='';
        $form = $this->createFormBuilder()
            ->add('movieName',TextType::class,['attr' => ['class' => 'form-control form-control-lg form-control-borderless' , 'type'=>'search', 'placeholder'  => 'Search for a movie']])
            ->add('Search', SubmitType::class, ['attr' => [ 'class'=>'btn btn-lg btn-success', 'type'=> 'submit', 'name'=>'btnA']])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted()){

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://www.omdbapi.com/?' . 't=' . $form['movieName']->getData() . '&apikey=' . 'f40e1370'  );
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-type: application/json']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $response = curl_exec($ch);
            curl_close($ch);


            $js = json_decode($response);

            $ss = $js;
            $rt = json_encode($ss);

            $url = 'http://localhost:8000/add/' . http_build_query(array($rt));
        }




            return $this->render('movie/index.html.twig', [
                'form' => $form->createView(),
                'js' => $js,
                'url' => $url
            ]);

    }
    /**
     * @Route("/add/{slug}", name="add_movie", methods={"GET"})
     *
     *
     */
    public function post(Request $request){

        $ss = json_decode(
            $request->getContent(),
            true
        );



        $movie = new Movie();

        $movie->setActors($ss['Actors']);
        $movie->setDirector($ss['Director']);
        $movie->setGenre($ss['Genre']);
        $movie->setName($ss['Title']);
        $movie->setPlot($ss['Plot']);
        $movie->setPoster($ss['Poster']);
        $movie->setRating($ss['imdbRating']);
        $movie->setRuntime($ss['Runtime']);
        $movie->setVotes($ss['imdbVotes']);
        $movie->setWriter($ss['Writer']);

        $em =$this->getDoctrine()->getManager();
        $em->persist($movie);
        $em->flush();

        $this->addFlash(
            'notice',
            'Movie Added'
        );

        return $this->redirect('/');
    }



}
